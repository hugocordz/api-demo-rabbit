FROM openjdk:11-slim

WORKDIR /vol/app

COPY ./target/*.jar app.jar

CMD ["java", "-jar", "app.jar"]