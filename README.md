# Getting Started

##Commands execution

- Project Build
    > mvn clean install
- Docker image build
    > docker build -t api-demo-rabbit . (the . is part of the command)
 
 __Note:__
    ```
    - Inside the rabbit image you have to create the queue with the name  rest.api.example
    ```
    
- In case you have to install the rabbitmq image usde the following commands
    > docker run -p 5672:5672 -p 15672:15672 rabbitmq:3.8.3-management
- After that command you can access localhost:15672 and create the queue rest.api.example                                                                       