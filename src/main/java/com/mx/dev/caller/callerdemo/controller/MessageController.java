package com.mx.dev.caller.callerdemo.controller;

import com.mx.dev.caller.callerdemo.service.PublishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class MessageController {

    @Autowired
    private PublishService publishService;

    @GetMapping(value = "/")
    private String messageEndpoint() {
      log.info("Starting request attention");
      return publishService.manageRequest();
    }

}
