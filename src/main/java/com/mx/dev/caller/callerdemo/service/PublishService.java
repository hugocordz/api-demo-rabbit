package com.mx.dev.caller.callerdemo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@Component
public class PublishService {

    @Autowired
    private Queue queue;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public String manageRequest() {
        try {
            log.info("Sending request message to rabbit");
            MessageProperties properties = new MessageProperties();
            Message message = new Message("Esta es una prueba".getBytes(), properties);
            Message response = rabbitTemplate.sendAndReceive(queue.getName(), message);
            String responseString = new String(response.getBody());
            log.info("Receiving response from rabbit: {}", responseString);
            return responseString;
        } catch (Exception e) {
            log.error("error sending info ", e);
        }
        return null;
    }

}
