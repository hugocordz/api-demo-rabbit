package com.mx.dev.caller.callerdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CallerDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CallerDemoApplication.class, args);
    }

}
